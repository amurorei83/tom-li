/*
 * Authored by Tom Li on 29/03/16.
 */

'use strict';

angular.module('amptest').factory("EmployeeService", ['Employee', function(Employee) {

//    'ngInject';

    return {

        getEmployees: function() {
            
            /* Normally this would be an ajax call, but since it's a script file... */
            var employees = [];
            data.forEach(function(employeeObj) {
                employees.push(new Employee(employeeObj));
            });
                  
            return {
                then : function(callback) {
                    callback(employees);
                }
            }
        }
    }

}]);