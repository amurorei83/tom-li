/*
 * Authored by Tom Li on 29/03/16.
 */

'use strict';

angular.module('amptest').controller('EmployeeController', ['$scope', 'EmployeeService', function($scope, employeeService) {

    'ngInject';

    $scope.employeeList  = [];

    employeeService.getEmployees().then(function(response) {
        $scope.employeeList = response;
    });

}]);