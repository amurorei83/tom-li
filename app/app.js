/*
 * Authored by Tom Li on 29/03/16.
 */

'use strict';

var amp = angular.module('amptest', []);

angular.element(document).ready(function() {

    angular.bootstrap(document, ['amptest'], {
        strictDi: true
    });

});