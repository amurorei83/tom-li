/*
 * Authored by Tom Li on 29/03/16.
 */

'use strict';

angular.module('amptest').factory('Employee', [function() {

    function Employee(data) {
        this.id = data.id || null;
        this.firstName = data.firstName || null;
        this.lastName = data.lastName || null;
        this.picture = data.picture || null;
        this.title = data.Title || null;
    };

    Object.defineProperty(Employee.prototype, 'imgUrl', {
        get: function() {
            return this.picture.replace('.jpg','.png');
        }
    });

    Object.defineProperty(Employee.prototype, 'name', {
        get: function() {
            return this.firstName;
        }
    });

    return Employee;

}]);
