/*
 * Authored by Tom Li on 29/03/16.
 */

describe('EmployeeService Unit Test', function() {

    beforeEach(angular.mock.module('amptest'));

    var employeeService,
        scope,
        expect = chai.expect;
        
    beforeEach(inject(function (_EmployeeService_) {
        employeeService = _EmployeeService_;
    }));
    
    it('Service should return Employee objects', function() {
        employeeService.getEmployees().then(function(employees){

            expect(employees.length).to.equal(6);
            expect(employees[0].constructor.name).to.equal("Employee");

        });
    });    

});