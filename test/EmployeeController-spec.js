/*
 * Authored by Tom Li on 29/03/16.
 */

'use strict';

describe('EmployeeController Unit Tests', function() {

    beforeEach(angular.mock.module('amptest'));   

    var employeeCtrl,
        scope,
        expect = chai.expect;

    beforeEach(inject(function ($controller, $rootScope, $injector) {
        
        scope = $rootScope.$new();
        
        //var $controller = $injector.get('$controller');
        
        employeeCtrl = $controller('EmployeeController', {
            $scope: scope
        });
        
    })); 

    it('should be initialised with Employees', function () {
        
        expect(scope.employeeList.length).to.equal(6);
        
    });

});